//Khai báo thư viện express
const express = require("express");

//khai báo thư viện path là thư viện đường dân
const path = require("path");
//Khai báo router
const { TimeRouter } = require("./app/router/time.router");
const { UrlRouter } = require("./app/router/url.router");

const app = express();
const port = 8000;

app.get("/",(req,res)=>{
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/view/giaodien.html"))
})

//sử dụng router
app.use(TimeRouter,UrlRouter);


app.listen(port, () => {
    console.log(`App đã chạy trên cổng : ${port}`);
})
