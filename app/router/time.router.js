//Khai báo một thư viện express
const express = require ('express');
//import middlerware
const {TimeMiddleware} = require("../middleware/time.middleware");
//tạo router
const TimeRouter = express.Router();
// Sử dụng router
TimeRouter.use(TimeMiddleware);

// exprot router 
module.exports = {TimeRouter};
