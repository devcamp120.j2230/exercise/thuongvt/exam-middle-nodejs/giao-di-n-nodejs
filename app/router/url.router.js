//Khai báo một thư viện express
const express = require ('express');
//import middlerware
const {URlMiddleware} = require("../middleware/url.middleware");
//tạo router
const UrlRouter = express.Router();
// sử dụng middlerware
UrlRouter.use(URlMiddleware);

// exprot router
module.exports = {UrlRouter};
